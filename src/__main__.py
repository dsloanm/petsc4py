# Author:  Lisandro Dalcin
# Contact: dalcinl@gmail.com

def main(args=None):
    import petsc4py.help
    petsc4py.help.help(args)

if __name__ == '__main__':
    main()
